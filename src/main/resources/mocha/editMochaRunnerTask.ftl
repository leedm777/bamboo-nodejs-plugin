[#-- @ftlvariable name="uiConfigBean" type="com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport" --]

[#assign addExecutableLink][@ui.displayAddExecutableInline executableKey='node' /][/#assign]
[@s.select cssClass="builderSelectWidget" labelKey='node.runtime' name='runtime'
list=uiConfigBean.getExecutableLabels('node')
extraUtility=addExecutableLink required=true /]

[@s.textfield labelKey='mocha.runner.runtime' name='mochaRuntime' cssClass="long-field" required=true /]
[@s.textfield labelKey='mocha.runner.testFiles' name='testFiles' cssClass="long-field" required=true /]
[@s.checkbox labelKey='mocha.runner.parseTestResults' name='parseTestResults' /]

[@ui.bambooSection titleKey='repository.advanced.option' collapsible=true
isCollapsed=!(arguments?has_content || environmentVariables?has_content || workingSubDirectory?has_content)]
    [@s.textfield labelKey='mocha.runner.arguments' name='arguments' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.env' name='environmentVariables' cssClass="long-field" /]
    [@s.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]
[/@ui.bambooSection]

