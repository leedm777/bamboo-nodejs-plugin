package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.plugins.nodejs.tasks.bower.BowerConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.testutils.TestBuildDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.BowerTaskComponent;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import org.junit.Test;

import java.util.Map;

public class BowerTaskTest extends AbstractNodeTaskTest
{
    @Test
    public void testTask() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestBuildDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // install required modules using npm
        final Map<String, String> npmTaskConfig = ImmutableMap.of(NpmConfigurator.COMMAND, "install bower");
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install", npmTaskConfig);

        // add Bower 'install' task
        final Map<String, String> bowerTaskConfig = ImmutableMap.of(BowerConfigurator.COMMAND, "install");
        taskConfigurationPage.addNewTask(BowerTaskComponent.TASK_NAME, BowerTaskComponent.class, "bower install", bowerTaskConfig);

        backdoor.plans().triggerBuild(plan.getPlanKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getPlanKey(), 1);
    }
}